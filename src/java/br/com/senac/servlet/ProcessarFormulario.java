package br.com.senac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.MultiPartEmail;

@WebServlet(name = "ProcessarFormulario", urlPatterns = {"/paginas/processarFormularioSugestao.do"})
public class ProcessarFormulario extends HttpServlet {

    private String myPassword = "";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean isInformacoesValidas = false;
        boolean isReceberInformacoes = false;

        String nome = request.getParameter("nome");
        String assunto = request.getParameter("assunto");  //S / C / E
        String mensagem = request.getParameter("mensagem");
        String informacoesValidas = request.getParameter("informacoes");
        isInformacoesValidas = informacoesValidas.equals("S");
        String receberInformacoes = request.getParameter("receberInformacoes");
        isReceberInformacoes = receberInformacoes != null;

        System.out.println("Nome:" + nome);
        System.out.println("Assunto:" + assunto);
        System.out.println("Mensagem:" + mensagem);
        System.out.println("Informacoes Validas:" + isInformacoesValidas);
        System.out.println("ReceberInformacoes:" + isReceberInformacoes);

        String assuntoMensagem = null;
        if (assunto.equalsIgnoreCase("s")) {
            assuntoMensagem = "Sugestão";
        } else if (assunto.equalsIgnoreCase("e")) {
            assuntoMensagem = "Elogio";
        } else if (assunto.equalsIgnoreCase("c")) {
            assuntoMensagem = "Crítica";
        }
        String infoV = null;
        if (isInformacoesValidas) {
            infoV = "Sim";
        } else {
            infoV = "Não";
        }

        String receberInformacoesV = null;
        if (isReceberInformacoes) {
            receberInformacoesV = "Sim";
        } else {
            receberInformacoesV = "Não";
        }

        String mensagemEnvio = String.format(
                "Nome: %s\n"
                + "Assunto: %s\n"
                + "Informações Validas ? %s\n"
                + "Deseja receber informações: %s\n"
                + "Mensagem:\n %s", nome, assuntoMensagem, infoV, receberInformacoesV, mensagem);

        String mensagemEnvio2
                = "Nome: " + nome + "\n"
                + "Assunto: " + assuntoMensagem + "\n"
                + "Informações Validas ? " + infoV + "\n"
                + "Deseja receber informações: " + receberInformacoesV + "\n"
                + "Mensagem:\n " + mensagemEnvio;

        /*Assunto: UC14 - Seu nome Completo
                
Corpo do E-mail :
Nome: Pedro Jorge
Assunto: Sugestão
Informações Validas ? Sim
Deseja receber informações: Sim
Mensagem:*/
        this.enviarEmail("issuesmaker@gmail.com", "Assunto: UC14 - Daniel Santiago", mensagemEnvio);
          this.enviarEmail("issuesmaker@gmail.com", "Assunto: UC14 - Daniel Santiago", mensagemEnvio2);

    }

    private void enviarEmail(String destino, String assunto, String mensagem) {

        String myEmailId = "issuesmaker@gmail.com";
        String senderId = destino;

        try {
            MultiPartEmail email = new MultiPartEmail();
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setHostName("smtp.gmail.com");
            email.setFrom(myEmailId);
            email.setSubject(assunto);
            email.setMsg(mensagem);
            email.addTo(senderId);
            email.setTLS(true);
            email.send();
            System.out.println("E-mail enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }

    }

}
